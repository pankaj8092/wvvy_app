import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import Login from '../Components/Login'
import Dashboard from '../Components/Dashboard'
const AppStack = createStackNavigator({

  Dashboard: {
    screen: Dashboard,
  },
},
  {
    unmountInactiveRoutes: true,
    defaultNavigationOptions: {
      header: null
    },
  },
);

const AuthStack = createStackNavigator({

  Login: {
    screen: Login,
  },
  Dashboard:{
    screen:Dashboard
  }
 
},

  {
    defaultNavigationOptions:
    {
      header: null,
    },
  }

);


export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: AuthStack,
    Auth: AuthStack,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));
